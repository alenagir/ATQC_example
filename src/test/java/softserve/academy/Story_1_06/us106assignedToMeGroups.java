package softserve.academy.Story_1_06;

import org.testng.annotations.*;
import softserve.academy.BaseTest;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.actions.GroupsPageActions;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.models.*;

import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

@Test(groups = {"teachersGroups"})
public class us106assignedToMeGroups {

    @BeforeTest
    public void setUpForOrdinaryTest() {

        setDriver()
                .manage()
                .window()
                .maximize();

        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(new User("sasha", "1234"))
                .waitPageLoaded();
    }

    @BeforeMethod
    public void createTeachesAndAssignedGroups() {

        new AdminPanelActions()
                .openAdminPanel()
                .openUserTab()
                .openCreateModal()
                .fillAllUserFields(UserDataProvider.teacher106)
                .submitCreateEdit()
                .openGroupTab()
                .openCreateModal()
                .fillAllGroupFields(GroupDataProvider.assignedToTeacher1)
                .submitCreateEdit()
                .openCreateModal()
                .fillAllGroupFields(GroupDataProvider.assignedToTeacher2)
                .submitCreateEdit()
                .logOut()
                .waitLogInPageLoad()
                .login(UserDataProvider.teacher106)
                .waitPageLoaded();
    }

    @Test
    public void teachersAbilityToSeeAllAssignedToThemGroups() {

        new AdminPanelActions()
                .openAdminPanel()
                .openGroupTab()
                .verifyUserGroupsInAdminAndGroupPageTheSame(UserDataProvider.teacher106);
    }

    @AfterMethod
    public void deleteTeachesAndAssignedGroups() {
        new AdminPanelActions()
                .openAdminPanel()
                .openUserTab()
                .deleteRowByName(UserDataProvider.teacher106.getLogin())
                .openGroupTab()
                .deleteRowByName(GroupDataProvider.assignedToTeacher1.getName())
                .deleteRowByName(GroupDataProvider.assignedToTeacher2.getName());
    }

    @AfterTest
    public void guitDriver() {
        getDriver().quit();
    }
}
