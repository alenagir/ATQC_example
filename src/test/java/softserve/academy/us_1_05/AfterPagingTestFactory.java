package softserve.academy.us_1_05;

import org.testng.annotations.Factory;

public class AfterPagingTestFactory {
    @Factory
    public Object[] factoryMethod() {
        return new Object[]{
                new SelectLocationAfterPaging("sasha", "1234"),
                new SelectLocationAfterPaging("dmytro", "1234"),
                new SelectLocationAfterPaging("admin", "1234")
        };
    }

}
