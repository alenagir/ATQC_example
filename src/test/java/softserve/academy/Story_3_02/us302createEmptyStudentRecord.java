package softserve.academy.Story_3_02;
import org.testng.annotations.*;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.actions.StudentsPageActions;
import softserve.academy.modal_windows.MWEditStudentlistActions;
import softserve.academy.models.Student;
import softserve.academy.models.User;

import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

@Test(groups = {"createStudentsList"})
public class us302createEmptyStudentRecord {
    @BeforeTest(description = "Preconditions for create Students List")
    public void setUpForOrdinaryTest() {

        setDriver()
                .manage()
                .window()
                .maximize();
        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(new User("dmytro", "1234"))
                .waitPageLoaded()
                .openStudentsPage()
                .selectGroup("DP-094-MQC");
    }

    @BeforeMethod
    public void setStudPage(){
        new StudentsPageActions()
                .editStudentList();
    }

    @Test(description = "User story 3-02:  Coordinator is on the “Group” page -> " +
            "Main menu -> Navigate to “Students” pageUser -> select group in group list -> Cogwheel -> " +
            "‘Edit Student list” modal window -> “Edit Student list”  ->  “Edit Student list” modal window -> " +
            "“+” for editing should open “Create/Edit student” modal window -> creating sdudents -> ")
    public void createNewStudents() {
        new MWEditStudentlistActions()
                .clickCreateStudent()
                .verifyCreateEmptyStudent()
                .clickClose()
                .assertAll();
    }

    @AfterMethod
    public void closeMWEditStudentList() {
        new MWEditStudentlistActions()
                .clickIconExit();

    }

    @AfterTest(alwaysRun = true)
    public void tearDown() {
        new StudentsPageActions()
                .logOut();
        getDriver().quit();
    }
}
