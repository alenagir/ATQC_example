package softserve.academy.Story_3_02;

import org.testng.annotations.*;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.actions.StudentsPageActions;
import softserve.academy.modal_windows.MWEditStudentlistActions;
import softserve.academy.models.GroupDataProvider;
import softserve.academy.models.Student;
import softserve.academy.models.User;
import softserve.academy.models.Utils;

import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;
@Test(groups = {"createStudentsList"})
public class us302createStudentRecord {

    @BeforeTest(description = "Preconditions for create Students List")
    public void setUpForOrdinaryTest() {

        setDriver()
                .manage()
                .window()
                .maximize();
        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(new User("dmytro", "1234"))
                .waitPageLoaded()
                .openStudentsPage()
                .selectGroup("DP-094-MQC");
    }

    @BeforeMethod
    public void setStudPage(){
        new StudentsPageActions()
              .editStudentList();
    }

    @DataProvider(name = "provideStudentData")
    public Object[][] groupStudents() {
        return new Object[][]{
                {new Student("Irina", "Svitlenko", 0, 2)},
                {new Student("Maksim", "Nikitghenko", 1, 3)},
                {new Student("Alina", "Titarenko", 100, 2)},
                {new Student("Tetyana", "Vasilenko", 200, 3)},
                {new Student("Dmitro", "Zinchenko", 250, 4)},
                {new Student("Oksana", "Zelenko", 300, 5)},
                {new Student("Evgen", "Franchuk", 350, 4)},
                {new Student("Pavlo", "Visochin", 400, 3)},
                {new Student("Vladislava", "Dziga", 450, 2)},
                {new Student("Pak", "Tok", 500, 2)},
        };
    }

    @Test(dataProvider = "provideStudentData", description = "User story 3-02:  Coordinator is on the “Group” page -> " +
            "Main menu -> Navigate to “Students” pageUser -> select group in group list -> Cogwheel -> " +
            "‘Edit Student list” modal window -> “Edit Student list”  ->  “Edit Student list” modal window -> " +
            "“+” for editing should open “Create/Edit student” modal window -> creating sdudents")
    public void createNewStudents(Student student) {
        new MWEditStudentlistActions()
                .clickCreateStudent()
                .setAllStudentRequiredFields(student)
                .verifyPresenceStudent(student)
                .assertAll();
    }

    @AfterMethod
    public void closeMWEditStudentList() {
        new MWEditStudentlistActions()
                .clickIconExit();

    }

    @AfterTest(alwaysRun = true)
    public void tearDown() {
        new StudentsPageActions()
        .logOut();
        getDriver().quit();
    }
}
