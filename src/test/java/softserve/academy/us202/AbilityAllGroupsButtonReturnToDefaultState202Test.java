package softserve.academy.us202;

import org.testng.annotations.Test;
import softserve.academy.pages.TopMenu;
import softserve.academy.us101.BaseTest;

public class AbilityAllGroupsButtonReturnToDefaultState202Test extends BaseTest {


	@Test(groups = {"schedulePage"}, description = "test verify after using allGroups button ability to return " +
			"the allGroups button to default state  after deselecting any group's tab" )
	public void verifyDefaultStateButton() {

		new TopMenu()
				.goToSchedulePage()
				.clickCurrentGroupTab()
				.clickAllGroupsButton()
				.isSelectedAll()
				.selectRandomGroup()
				.verifyDefaultStateAllGroupsButton()
				.assertAll();

	}
}
