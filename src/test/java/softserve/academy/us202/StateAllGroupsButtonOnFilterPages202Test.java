package softserve.academy.us202;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.SchedulePageActions;
import softserve.academy.models.Filter;
import softserve.academy.pages.TopMenu;
import softserve.academy.us101.BaseTest;

public class StateAllGroupsButtonOnFilterPages202Test extends BaseTest {
	@DataProvider(name = "filterButton")
	public static Object[][] authorizationData() {
		return new Object[][]{
				{Filter.FINISHED},
				{Filter.CURRENT},
				{Filter.PLANNED}
		};

	}

	@Test(groups = {"schedulePage2"}, description = "test allGroups button on filter pages after pressing on every" +
			" page", dataProvider = "filterButton")
	public void testStateAllGroupsButtonOnFilterPages(Filter filter) {

		new SchedulePageActions()
				.clickFilter(filter)
				.verifyDefaultStateAllGroupsButton()
				.clickAllGroupsButton()
				.verifyStatePressedAllGroupsButton()
				.assertAll();

	}

}
