package softserve.academy.us107;

import org.testng.annotations.*;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.models.Location;

import static softserve.academy.models.GroupDataProvider.*;
import static softserve.academy.models.UserDataProvider.admin107;
import static softserve.academy.models.UserDataProvider.coordinator107;
import static softserve.academy.models.UserDataProvider.teacher107;
import static softserve.academy.models.Utils.*;

public class Story107EditGroupPermissionsTest {

    @Test(description = "User story 1-07: Teacher can edit group assigned to him in his location")
    public void teacherAssignedInLocation() {
        new LoginPageActions()
                .login(teacher107)
                .selectGroup(us107TeacherRivne.getName())
                .verifyEditGroupButtonIsDisplayed()
                .assertAll();
    }

    @Test(description = "User story 1-07: Coordinator can edit group assigned to him in his location")
    public void coordinatorAssignedInLocation() {
        new LoginPageActions()
                .login(coordinator107)
                .selectGroup(us107CoordinatorKyiv.getName())
                .verifyEditGroupButtonIsDisplayed()
                .assertAll();
    }

    @Test(description = "User story 1-07: Coordinator can edit group NOT assigned to him in his location")
    public void coordinatorNotAssignedInLocation() {
        new LoginPageActions()
                .login(coordinator107)
                .selectGroup(us107AdminKyiv.getName())
                .verifyEditGroupButtonIsDisplayed()
                .assertAll();
    }

    @Test(description = "User story 1-07: Admin can edit group assigned to him in his location")
    public void adminAssignedInLocation() {
        new LoginPageActions()
                .login(admin107)
                .selectGroup(us107AdminLviv.getName())
                .verifyEditGroupButtonIsDisplayed()
                .assertAll();
    }

    @Test(description = "User story 1-07: Admin can edit group assigned to him NOT in his location")
    public void adminAssignedNotInLocation() {
        new LoginPageActions()
                .login(admin107)
                .openSelectLocationWindow()
                .clearChecks()
                .selectLocation(Location.KYIV)
                .save()
                .selectGroup(us107AdminKyiv.getName())  //TODO navigate to Kyiv
                .verifyEditGroupButtonIsDisplayed()
                .assertAll();
    }

    @Test(description = "User story 1-07: Admin can edit group NOT assigned to him in his location")
    public void adminNotAssignedInLocation() {
        new LoginPageActions()
                .login(admin107)
                .selectGroup(us170TeacherLviv.getName())
                .verifyEditGroupButtonIsDisplayed()
                .assertAll();
    }

    @Test(description = "User story 1-07: Admin can edit group assigned NOT to him NOT in his location")
    public void adminNotAssignedNotInLocation() {
        new LoginPageActions()
                .login(admin107)
                .openSelectLocationWindow()
                .clearChecks()
                .selectLocation(Location.KYIV)
                .save()
                .selectGroup(us107CoordinatorKyiv.getName())  //TODO navigate to Kyiv
                .verifyEditGroupButtonIsDisplayed()
                .assertAll();
    }

    //**************Negative Tests******//
    @Test(description = "User story 1-07: Teacher can't edit group NOT assigned to him in his location")
    public void teacherNotAssignedInLocation() {
        new LoginPageActions()
                .login(teacher107)
                .selectGroup(us107CoordinatorRivne.getName())
                .verifyEditGroupButtonNotDisplayed()
                .assertAll();
    }

    @Test(description = "User story 1-07: Teacher can't edit group assigned to him NOT in his location")
    public void teacherAssignedNotInLocation() {
        new LoginPageActions()
                .login(teacher107)
                .openSelectLocationWindow()
                .clearChecks()
                .selectLocation(Location.LVIV)
                .save()
                .selectGroup(us170TeacherLviv.getName())
                .verifyEditGroupButtonNotDisplayed()
                .assertAll();
    }

    @Test(description = "User story 1-07: Teacher can't edit group NOT assigned to him NOT in his location")
    public void teacherNotAssignedNotInLocation() {
        new LoginPageActions()
                .login(teacher107)
                .openSelectLocationWindow()
                .clearChecks()
                .selectLocation(Location.LVIV)
                .save()
                .selectGroup(us107AdminKyiv.getName())
                .verifyEditGroupButtonNotDisplayed()
                .assertAll();
    }

    @Test(description = "User story 1-07: Coordinator can't edit group assigned to him NOT in his location")
    public void coordinatorAssignedNotInLocation() {
        new LoginPageActions()
                .login(coordinator107)
                .openSelectLocationWindow()
                .clearChecks()
                .selectLocation(Location.DNIPRO)
                .save()
                .selectGroup(us107CoordinatorRivne.getName())
                .verifyEditGroupButtonNotDisplayed()
                .assertAll();
    }

    @Test(description = "User story 1-07: Admin can't edit group NOT assigned to him NOT in his location")
    public void coordinatorNotAssignedNotInLocation() {
        new LoginPageActions()
                .login(coordinator107)
                .openSelectLocationWindow()
                .clearChecks()
                .selectLocation(Location.LVIV)
                .save()
                .selectGroup(us107AdminLviv.getName())
                .verifyEditGroupButtonNotDisplayed()
                .assertAll();
    }

    @BeforeClass
    public void login() {
        setDriver().manage().window().maximize();
        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(admin107)
                .waitPageLoaded();
    }

    @BeforeMethod
    public void logoutAfterPrevious() {
        getDriver().navigate().to(LOGOUT_URL);
    }

    @AfterClass
    public void quit() {
        getDriver().quit();
    }
}
