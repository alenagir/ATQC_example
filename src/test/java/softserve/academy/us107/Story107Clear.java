package softserve.academy.us107;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.models.Group;
import softserve.academy.models.GroupDataProvider;

import static softserve.academy.models.UserDataProvider.admin107;
import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

public class Story107Clear {
    AdminPanelActions adminPage;

    @BeforeClass
    public void login() {
        setDriver().manage().window().maximize();
        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(admin107)
                .waitPageLoaded();
        adminPage = new AdminPanelActions()
                .openAdminPanel()
                .openGroupTab();
    }

    @Test(dataProvider = "GroupDataProvider")
    public void tearDown(Group group) {
        adminPage
                .deleteRowByName(group.getName())
                .assertAll();
    }

    @AfterClass
    public void quit() {
        getDriver().quit();
    }

    @DataProvider(name = "GroupDataProvider")
    public Object[][] groupCreateData() {
        return new Object[][]{
                {GroupDataProvider.us107TeacherRivne},
                {GroupDataProvider.us107CoordinatorKyiv},
                {GroupDataProvider.us170TeacherLviv},
                {GroupDataProvider.us107AdminLviv},
                {GroupDataProvider.us107AdminKyiv},
                {GroupDataProvider.us107CoordinatorRivne}
        };
    }
}
