package softserve.academy.us101;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.LoginPageActions;

public class KeyboardEventOfEscFunctionality101Test extends BaseTest {


	@DataProvider(name = "data")
	public static Object[][] loginData() {
		return new Object[][]{
				{"dmytro", "1234", By.name("login")},
				{"dmytro", "1234", By.name("password")},
				{"", "1234", By.name("password")},
				{"", "1234", By.name("login")},
				{"artur", "", By.name("password")},
				{"artur", "", By.name("login")}};

	}


	//button esc functionality
	@Test(groups = {"loginPage"}, description = "test мукшан esk key functionality to clear all fields",
			dataProvider = "data")
	void testEscapeKeyEventAndStateOfSubmitButton(String login, String password, By locator) {

		new LoginPageActions()
				.openLoginPage()
				.waitLogInPageLoad()
				.fillLoginField(login)
				.fillPasswordField(password)
				.pressEscKey(locator)
				.verifyEmptyLoginField()
				.verifyEmptyPasswordField()
				.assertAll();

	}
}
