package softserve.academy.us101;

import org.openqa.selenium.By;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.LoginPageActions;

public class StateOfSubmitButton101Test extends BaseTest {

	@DataProvider(name = "data")
	public static Object[][] loginData() {
		return new Object[][]{
				{"dmytro", "1234", By.name("login")},
				{"dmytro", "1234", By.name("password")},
				{"", "1234", By.name("password")},
				{"", "1234", By.name("login")},
				{"artur", "", By.name("password")},
				{"artur", "", By.name("login")}};

	}


	//button esc functionality
	@Test(groups = {"loginPage"}, description = "test verify state of submit button", dataProvider = "data")
	void testStateOfSubmitButton(String login, String password, By locator) {

		new LoginPageActions()
				.openLoginPage()
				.waitLogInPageLoad()
				.verifyDisabledOfLoginButton()
				.fillLoginField(login)
				.verifyDisabledOfLoginButton()
				.fillPasswordField(password)
				.verifyEnabledOfLoginButton()
				.pressEscKey(locator)
				.verifyDisabledOfLoginButton()
				.assertAll();

	}
}
