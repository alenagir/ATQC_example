package softserve.academy.AdminTests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.models.Group;
import softserve.academy.models.GroupDataProvider;


@Test(groups ={"admin", "feedback"})
public class AdminCreateGroupAndVerifyOnGroupPageTest extends AdminBaseTest {


    @Test(dataProvider = "GroupDataProvider")
    public void createDeleteGroupsAndVerifyOnGroupPage(Group group1, Group group2) {
        new AdminPanelActions()
                .openAdminPanel()
                .openGroupTab()
                .openCreateModal()
                .fillAllGroupFields(group1)
                .submitCreateEdit()
                .openCreateModal()
                .fillAllGroupFields(group2)
                .submitCreateEdit()
                .escapeToGroupPage();


    }

    @DataProvider(name = "GroupDataProvider")
    public Object[][] groupCreateData() {
        return new Object[][]{
                {GroupDataProvider.DP_Current1, GroupDataProvider.DP_Current2}

        };
    }
}