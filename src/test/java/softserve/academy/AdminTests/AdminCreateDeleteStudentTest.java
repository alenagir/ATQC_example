package softserve.academy.AdminTests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.models.Student;
import softserve.academy.models.StudentDataProvider;
import softserve.academy.models.User;
import softserve.academy.models.UserDataProvider;

@Test(groups = {"admin", "funcCreateDelete", "student"})
public class AdminCreateDeleteStudentTest extends AdminBaseTest{

    @Test(dataProvider = "StudentDataProvider")
    public void createDeleteStudents(Student student) {
        new AdminPanelActions()
                .openAdminPanel()
                .openStudentTab()
                .openCreateModal()
                .fillAllStudentFields(student)
                .submitCreateEdit()
                .verifySoftRowWithNamePresentInTab(student.getLastName(), "students")
                .verifyCellWithTextPresentInRow(student.getName())
                .verifyCellWithTextPresentInRow(student.getLastName())
                .verifyCellWithTextPresentInRow(student.getGroupID())
                .verifyCellWithTextPresentInRow(student.getEnglishLevel1())
                .verifyCellWithTextPresentInRow(student.getEntryScore1())
                .verifyCellWithTextPresentInRow(student.getApprovedBy1())
                .deleteRowByName(student.getLastName())
                .verifyRowWithNameNotPresentInTab(student.getLastName(), "users")
                .assertAll();

    }


    @DataProvider(name = "StudentDataProvider")
    public Object[][] userCreateData() {
        return new Object[][]{
                {StudentDataProvider.student1},
                {StudentDataProvider.student2}
        };
    }
}
