package softserve.academy.AdminTests;

import org.testng.annotations.*;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.models.GroupDataProvider;
import softserve.academy.models.Student;
import softserve.academy.models.StudentDataProvider;
import softserve.academy.models.User;
;

import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

@Test(groups ={"admin", "funcEdit"})
public class AdminEditStudentTest {

       @Test(dataProvider = "StudentDataProviderForEditTest")
        public void editStudents(Student studentBeforeEdit, Student studentAfterEdit){
            new AdminPanelActions()
                    .openAdminPanel()
                    .openStudentTab()
                    .openEditModal(studentBeforeEdit.getLastName())
                    .editStudentField("lastName", studentAfterEdit.getLastName())
                    .closeCreateEdit()
                    .verifyRowWithNameNotPresentInTab(studentAfterEdit.getLastName(), "students")
                    .openEditModal(studentBeforeEdit.getLastName())
                    .editUserField("groupId", studentAfterEdit.getGroupID())
                    .editUserField("name", studentAfterEdit.getName())
                    .editUserField("lastName", studentAfterEdit.getLastName())
                    .editUserField("englishLevel", studentAfterEdit.getEnglishLevel1())
                    .editUserField("entryScore", studentAfterEdit.getEntryScore1())
                    .editUserField("approvedBy", studentAfterEdit.getApprovedBy1())
                    .submitCreateEdit()
                    .verifySoftRowWithNamePresentInTab(studentAfterEdit.getLastName(), "students")
                    .verifyCellWithTextPresentInRow(studentAfterEdit.getName())
                    .verifyCellWithTextPresentInRow(studentAfterEdit.getGroupID())
                    .verifyCellWithTextPresentInRow(studentAfterEdit.getEnglishLevel1())
                    .verifyCellWithTextPresentInRow(studentAfterEdit.getEntryScore1())
                    .verifyCellWithTextPresentInRow(studentAfterEdit.getApprovedBy1())
                    .verifyAll();

        }

        @DataProvider(name="StudentDataProviderForEditTest")
        public Object[][] studentEditData() {
            return new Object[][]{
                    {StudentDataProvider.studentBeforeEdit, StudentDataProvider.studentAfterEdit}
            };
        }


    User userLogIn = new User("alena", "1234");
    @BeforeTest(groups = {"funcEdit"})
    public void startStudentToEdit() {
        setDriver()
                .manage()
                .window()
                .maximize();

        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(new User(userLogIn.getLogin(), userLogIn.getPassword()))
                .waitPageLoaded();
    }
    @BeforeMethod(groups = {"funcEdit"})
    public void createStudentToEdit(){
        new AdminPanelActions()
                .openAdminPanel()
                .openStudentTab()
                .openCreateModal()
                .fillAllStudentFields(StudentDataProvider.studentBeforeEdit)
                .submitCreateEdit();
    }

    @AfterMethod(alwaysRun = true, groups = {"funcEdit"})
    public void deleteStudentAfterEdit() {
        new AdminPanelActions()
                .openAdminPanel()
                .openStudentTab()
                .deleteRowByName(StudentDataProvider.studentAfterEdit.getName());
    }
    @AfterTest(alwaysRun = true, groups = {"funcEdit"})
    public void quit(){
        getDriver().quit();
    }

}
