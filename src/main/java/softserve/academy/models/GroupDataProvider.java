package softserve.academy.models;

public class GroupDataProvider {
    //FOR Admin ONLY****************************************************************************************************
    public static final Group DP_Current1 = new Group("DP-700", Location.DNIPRO.getValue(),
            true, Direction.ATQC.getValue(), "10/01/2018",
            "12/28/2018", "Girenko", Expert.KARASIK.getValue(), GroupStage.IN_PROGRESS.getValue());
    public static final Group DP_Current2 = new Group("DP-701", Location.DNIPRO.getValue(),
            true, Direction.JAVA.getValue(), "10/01/2018",
            "12/28/2018", "Girenko", Expert.KARASIK.getValue(), GroupStage.IN_PROGRESS.getValue());

    public static final Group groupBeforeEdit = new Group("DP-702", Location.DNIPRO.getValue(),
            false, Direction.WEBUI.getValue(), "10/01/2018",
            "12/28/2018", "Girenko", Expert.KARASIK.getValue(), GroupStage.IN_PROGRESS.getValue());
    public static final Group groupAfterEdit = new Group("DP-800", Location.IVANO_FRANKIVSK.getValue(),
            true, Direction.ISTQB.getValue(), "12/01/2018",
            "01/28/2019", "Reuta", Expert.RYBAKOV.getValue(), GroupStage.PLANNED.getValue());

    //For 106 User Story*********************************************************************************************

    public static final Group assignedToTeacher1 = new Group("DP-019-MQC",Location.DNIPRO.getValue(),
            true,Direction.MQC.getValue(),"20.10.2018", "20.12.2018",
            "T. Nikolaiev","O.Lozoviagin",GroupStage.IN_PROGRESS.getValue());

    public static final Group assignedToTeacher2 = new Group("DP-019-JAVA",Location.DNIPRO.getValue(),
            false,Direction.JAVA.getValue(),"10.02.2018", "10.10.2018",
            "T. Nikolaiev","O.Lozoviagin",GroupStage.IN_PROGRESS.getValue());


    // ************ For 107 User Story *******
    public static final Group us107TeacherRivne = new Group("Rv-434",Location.RIVNE.getValue(),true,Direction.JAVA.getValue(),
            "10.02.2018", "10.10.2018","A. Teacher","O. Reuta",GroupStage.IN_PROGRESS.getValue());
    public static final Group us107CoordinatorKyiv = new Group("Kv-632",Location.KYIV.getValue(),true,Direction.JAVA.getValue(),
            "10.02.2018", "10.10.2018","D. Coordinator","O. Reuta",GroupStage.IN_PROGRESS.getValue());
    public static final Group us170TeacherLviv = new Group("Lv-532",Location.LVIV.getValue(),true,Direction.JAVA.getValue(),
            "10.02.2018", "10.10.2018","A. Teacher","O. Reuta",GroupStage.IN_PROGRESS.getValue());
    public static final Group us107AdminLviv = new Group("Lv-531",Location.LVIV.getValue(),true,Direction.JAVA.getValue(),
            "10.02.2018", "10.10.2018","B. Admin","O. Reuta",GroupStage.IN_PROGRESS.getValue());
    public static final Group us107AdminKyiv = new Group("Kv-630",Location.KYIV.getValue(),true,Direction.JAVA.getValue(),
            "10.02.2018", "10.10.2018","B. Admin","O. Reuta",GroupStage.IN_PROGRESS.getValue());
    public static final Group us107CoordinatorRivne = new Group("Rv-433",Location.RIVNE.getValue(),true,Direction.JAVA.getValue(),
            "10.02.2018", "10.10.2018","D. Coordinator","O. Reuta",GroupStage.IN_PROGRESS.getValue());


    //For 302 User Story*********************************************************************************************
    public static final Group forFillingStudentsList = new Group("DP-919-MQC",Location.DNIPRO.getValue(),
            true,Direction.MQC.getValue(),"02.15.2019", "05.10.2019",
            "T. Nikolaiev","O.Lozoviagin",GroupStage.PLANNED.getValue());
}
