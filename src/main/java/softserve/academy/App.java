package softserve.academy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.actions.GroupsPageActions;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.models.*;
import softserve.academy.pages.GroupsPage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

public class App {

    public static void main(String[] args) {

        User user=new User("Taras","Nikolaiev", Role.TEACHER.getValue(),
                Location.DNIPRO.getValue(),"","taras","1234" );
        setDriver()
                .manage()
                .window()
                .maximize();

        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(user.getLogin(), user.getPassword())
                .waitPageLoaded();

   
        getDriver().quit();
    }



}
