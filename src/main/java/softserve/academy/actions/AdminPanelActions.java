package softserve.academy.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import softserve.academy.models.User;
import softserve.academy.pages.adminPanel.AdminPanel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static org.testng.Assert.assertEquals;
import static softserve.academy.models.Utils.ADMIN_POSTFIX;
import static softserve.academy.models.Utils.BASE_URL;
import static softserve.academy.models.Utils.getDriver;

public class AdminPanelActions extends BaseActions {
    protected SoftAssert sa = new SoftAssert();
    AdminPanel adminPanel = new AdminPanel();
    WebElement addButton;
    List<WebElement> actualTable;
    WebElement actualRow;
    List<WebElement> actualCells;
    List<String> groupNames = new ArrayList<>();
    String step;

    public AdminPanelActions openAdminPanel() {
        getDriver().get(BASE_URL + ADMIN_POSTFIX);
        waitVisible(adminPanel.header);
        LOGGER.log(Level.INFO, "Wait for opening Admin Panel...");
        return this;
    }

    public AdminPanelActions openUserTab() {
        waitClickable(adminPanel.usersTab).click();
        return this;
    }

    public AdminPanelActions openGroupTab() {
        waitClickable(adminPanel.groupsTab).click();
        return this;
    }

    public AdminPanelActions openStudentTab() {
        waitClickable(adminPanel.studentsTab).click();
        return this;
    }

    public GroupsPageActions escapeToGroupPage(){
        moveTo(adminPanel.escapeHomeButton).click();
        wait.until(ExpectedConditions.urlContains("Group"));
        return new GroupsPageActions();
    }

    //*************************************************************************************************************

    public AdminModalActions openCreateModal() {
        if (moveTo(adminPanel.addUser).isDisplayed()) {
            addButton = adminPanel.addUser;
        } else if (moveTo(adminPanel.addGroup).isDisplayed()) {
            addButton = adminPanel.addGroup;
        } else if (moveTo(adminPanel.addStudent).isDisplayed()) {
            addButton = adminPanel.addStudent;
        }
        addButton.click();
        return chooseModalBy(addButton.getText());
    }

    public AdminModalActions openEditModal(String rowName) {
        adminPanel.edit = getDriver().findElement
                (By.xpath(".//*[text()='" + rowName + "']/following-sibling::td[last()]/button[@class='btn btn-info edit']"));
       waitClickable(adminPanel.edit);
        moveTo(adminPanel.edit).click();
        return chooseModalBy(rowName);
    }

    private AdminModalActions chooseModalBy(String elementText) {
        waitVisible(adminPanel.modalWindow);
        String ID = getDriver().findElement(By.xpath(".//*[text()='" + elementText + "']/ancestor::div[@id]")).getAttribute("id");

        return new AdminModalActions(ID);
    }

    //************************************************************************************************************
    public AdminPanelActions deleteRowByName(String rowName){
        WebElement delete=getDriver().findElement
                (By.xpath(".//tr/td[text()='"+rowName+"']/following-sibling::td/button[@class='btn btn-danger delete']"));
        moveTo(delete).click();
        wait.until(ExpectedConditions.invisibilityOf(delete));
        return this;
    };
    public AdminPanelActions deleteRowByTextAndFollowingText(String text, String followingText){
        WebElement delete=getDriver().findElement
                (By.xpath("//*[contains(.,'" +text+ "')]/following-sibling::td[contains(.,'" +followingText+ "')]/" +
                        "following-sibling::td/button[@class='btn btn-danger delete']"));
        moveTo(delete).click();
        wait.until(ExpectedConditions.invisibilityOf(delete));
        return this;
    };

    //************************************************************************************************************

    //Table Work private

    private List<WebElement> returnTableByTabName(String tabName){
        actualTable = getDriver().findElements
                (By.xpath("//*[@id='"+tabName+"']/div/table/tbody/tr"));
        return  actualTable;
    }

    private WebElement returnActualRowByTabNameAndRowText(String tabName, CharSequence rowText) {
        actualRow=null;
        returnTableByTabName(tabName);
        for (WebElement row : actualTable) {
            if (row.getText().contains(rowText)) {
                actualRow = row;
                break;
            }
        }
        actualCells=actualRow.findElements(By.tagName("td"));
        return actualRow;
    }

    private boolean findRowByTabNameAndRowText(String tabName, CharSequence rowText) {
        actualTable=returnTableByTabName(tabName);
        boolean isPresent=false;
        for (WebElement row : actualTable) {
            if (row.getText().contains(rowText)) {
                isPresent=true;
                break;
            }
        }return isPresent;
    }

    private WebElement returnCellByTabRowCellNames(String tabName, CharSequence rowName, CharSequence cellName) {
        adminPanel.rowCell=null;
        returnActualRowByTabNameAndRowText(tabName, rowName);
        adminPanel.rowCells=adminPanel.tableRow.findElements(By.tagName("td"));
        for (WebElement cell : adminPanel.rowCells) {
            if (cell.getText().contains(cellName)) {
                adminPanel.rowCell=cell;
                break;
            }
        }return adminPanel.rowCell;
    }



    public List<String> returnCellTextByTabRowNames(String tabName, CharSequence rowName, int cellNumber) {
        List<WebElement> fields = new ArrayList<>();

        actualTable=returnTableByTabName(tabName);

        for (WebElement row : actualTable) {
            if (row.getText().contains(rowName)) {
                fields = row.findElements(By.tagName("td"));
                groupNames.add(fields.get(cellNumber).getText());
            }
        }return groupNames;
    }

    private boolean findCellByTabRowCellNames(String tabName, CharSequence rowName, CharSequence cellName) {
        returnActualRowByTabNameAndRowText(tabName, rowName);
        adminPanel.rowCells=adminPanel.tableRow.findElements(By.tagName("td"));
        boolean isPresent=false;
        for (WebElement cell : adminPanel.rowCells) {
            if (cell.getText().contains(cellName)) {
                isPresent=true;
                break;
            }
        }return isPresent;
    }

    private boolean findCellByTextInActualRow(String text) {
       actualCells=actualRow.findElements(By.tagName("td"));
        boolean isPresent=false;
        for (WebElement cell : actualCells) {
            if (cell.getText().contains(text)) {
                isPresent=true;
                break;
            }
        }return isPresent;
    }
    private boolean findCheckBoxAttributeInRow(String rowName) {
        Boolean checked=null;
        WebElement checkBox = getDriver().
            findElement(By.xpath("//*[@id='groups']/descendant::td[contains(.,'"+rowName+"')]/following-sibling::td/input"));
        moveTo(checkBox);
        waitVisible(checkBox);
        if (checkBox.getAttribute("checked")==null) {checked=false;}
        else checked=true;
        return checked;
    }
    //************************************************************************************************************
    // Assertions
    //SOFT ASSERT


    public AdminPanelActions verifySoftInRowWithNameCheckBoxIs(String rowName, boolean expectedCheckboxState) {
        sa.assertEquals(findCheckBoxAttributeInRow(rowName), expectedCheckboxState,
               "checkBox does not equal expected in step " + step+": ");
        return this;
    }

    public VerifyCell verifySoftRowWithNamePresentInTab(String name, String tabName) {
        returnActualRowByTabNameAndRowText(tabName, name);
        sa.assertNotNull(actualRow, "cannot find "+name+" in "+tabName);
        return new VerifyCell();
    }
    public AdminPanelActions verifySoftRowWithNameNotPresentInTab(String name, String tabName) {
        sa.assertFalse(findRowByTabNameAndRowText(tabName, name), "found "+name+" in "+tabName);
        return this;
    }
    //This method is invoked only after the previous  verifyRowWithNamePresentInTab:
    public class VerifyCell {
       // protected SoftAssert sa = new SoftAssert();
        public VerifyCell verifyCellWithTextPresentInRow(String text) {
            sa.assertTrue(findCellByTextInActualRow(text), text+" is not found ");
            return this;
        }
        public AdminPanelActions verifyCellWithTextNotPresentInRow(String text) {
            sa.assertFalse(findCellByTextInActualRow(text), text+ " is present");
            return new AdminPanelActions();
        }

        public AdminPanelActions deleteRowByName(String rowName){
            WebElement delete=getDriver().findElement
                    (By.xpath(".//*[text()='"+rowName+"']/following-sibling::td/button[@class='btn btn-danger delete']"));
            moveTo(delete).click();
            wait.until(ExpectedConditions.invisibilityOf(delete));
            return new AdminPanelActions();
        };
        public VerifyCell verifyAll() {
            sa.assertAll();
            return this;
        }
    }

    public AdminPanelActions verifyAll() {
        sa.assertAll();
        return this;
    }

    //HARD ASSERT
    public AdminPanelActions verifyOpenedTab(String tabName) {
        String tHead = getDriver().findElement(By.xpath("//*[@id='" + tabName + "']/div/table/thead")).getText();
        CharSequence uniqueField = "";
        switch (tabName) {
            case "users":
                uniqueField = "login";
                break;
            case "groups":
                uniqueField = "direction";
                break;
            case "students":
                uniqueField = "entryScore";
        }
        Assert.assertTrue(tHead.contains(uniqueField), tabName+" is not opened");
        return this;
    }
    public AdminPanelActions verifyRowWithNamePresentInTab(String name, String tabName) {
        returnActualRowByTabNameAndRowText(tabName, name);
        Assert.assertNotNull(actualRow, "cannot find "+name+" in "+tabName);
        return this;
    }
    public AdminPanelActions verifyRowWithNameNotPresentInTab(String name, String tabName) {
        Assert.assertFalse(findRowByTabNameAndRowText(tabName, name), "found "+name+" in "+tabName);
        return this;
    }

    public GroupsPageActions verifyUserGroupsInAdminAndGroupPageTheSame(User user){
        List<String>adminGroups = returnCellTextByTabRowNames("groups",user.getLastName(),0);
        List<String>groupPageGroups = escapeToGroupPage().clickMyGroupBtn().getAllGroupsNames();
        Assert.assertTrue(adminGroups.containsAll(groupPageGroups));
        return new GroupsPageActions();
    }




    //*****************************************************************************************************************
    //UTILS

    public AdminPanelActions refresh() {
        getDriver().get(BASE_URL + ADMIN_POSTFIX);
        return this;
    }

    public AdminPanelActions pause(int i) {
        try {
            TimeUnit.SECONDS.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

    public AdminPanelActions step(String step){
        this.step=step;
        return this;
    }


}

